// ---- neural network navigation----

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "Aria.h"

using namespace std;

const double pi = acos(-1);
const double deg_to_rad =  pi/180;
double sin30 = sin(30*deg_to_rad);

string line;
string word;
string f_name;

double weight;
vector<double> weight_vec;
vector<vector<double>> weight_double;

double structure;
vector<double> structure_vec;
vector<vector<double>> structure_double;

int f_layer_index = 0;
int f_node_num_index = 1;
int f_node_val_index = 2;
int f_weight_val_index = 4;

int input_node_count_idx = 3;
int out_put_node_count_idx = 4;
int hidden_node_count_idx = 5;
int f_lambda_indx = 7;

ifstream csv_file;

int bias_node_num = 0;
int bias_node_val = 1;

int layer_count = 3;
int def_node_value = 0;

int input_node_count;   // -- read from data file
int out_put_node_count; // -- read from data file
int hidden_node_count;  // -- read from data file

double voltage;

const int input_layer_num = 0;
const int hidden_layer_num = 1;
const int out_put_layer_num = 2;

const double euler = 2.7182;

vector<double> output;

double lambda; // -- read from data file

double front_distance;
double left_distance;

double son_degree[8];
double s[8];

int sonarRange[8];

int lf;
int rght;

double left_speed;
double right_speed;

class perceptron
{
public:
    // ----- accessor functions -----

    double get_node_value() const 
    { 
        return node_value; 
    }
    double get_weights(int idx)
    {
        return weights[idx];
    }

    // ----- mutator functions -----

    void set_node_value(double new_val)
    {
        node_value = new_val;
    }
    void set_weights(double weights_in)
    {
        weights.push_back(weights_in);
    }

private:
    double node_value;
    vector<double> weights;
};

perceptron node;
vector<perceptron> node_array;
vector<vector<perceptron>> node_matrix;

// set neural network constants

void const_set()
{
    lambda = structure_double[0][f_lambda_indx];
    input_node_count = structure_double[0][input_node_count_idx];
    out_put_node_count = structure_double[0][out_put_node_count_idx];
    hidden_node_count = structure_double[0][hidden_node_count_idx];
}

// reading csv files

void csv_read(double element, vector<double> element_vec, vector<vector<double>> &double_convert, string f_name)
{
    csv_file.open(f_name);

    if (csv_file.is_open())
    {
        while (getline(csv_file, line))
        {
            istringstream line_stream(line);
            while (getline(line_stream, word, ','))
            {
                istringstream word_stream(word);
                word_stream >> element;
                element_vec.push_back(element);
            }
            double_convert.push_back(element_vec);
            element_vec.clear();
        }
    }
    double_convert.erase(double_convert.begin());
    csv_file.close();
}

// layer creation

void nn_layer_struct(int layer_size)
{
    for (int idx = 0; idx < layer_size; idx++)
    {
        node.set_node_value(1);
        node_array.push_back(node);
    }
    node_matrix.push_back(node_array);
    node_array.clear();
}

// fill node data based on input file

void fill_node_weights()
{

    double m = 0;
    for (int i = 0; i < node_matrix.size(); i++)
    {
        for (int j = 0; j < node_matrix[i].size(); j++)
        {
            if (i != input_layer_num)
            {
                for (int k = 0; k < weight_double.size(); k++)
                {
                    if (i == weight_double[k][f_layer_index] && j == weight_double[k][f_node_num_index])
                    {
                        node_matrix[i][j].set_weights(weight_double[k][f_weight_val_index]);
                    }
                }
            }
        }
    }
}

// feed forward function

void feed_forward()
{
    // updating hidden node layer and output layer node values values

    int start_layer = 1;

    for (int layer = start_layer; layer < node_matrix.size(); layer++)
    {
        int prv_layer = layer - 1;
        int layer_nd;
        double new_node_val = 1;
        double voltage = 0;
        double local_node_val;

        if (layer != out_put_layer_num)
        {
            layer_nd = 1;
        }
        else
        {
            layer_nd = 0;
        }

        for (layer_nd; layer_nd < node_matrix[layer].size(); layer_nd++)
        {
            voltage = 0;
            for (int prv_layer_nd = 0; prv_layer_nd < node_matrix[prv_layer].size(); prv_layer_nd++)
            {
                double multip = node_matrix[layer][layer_nd].get_weights(prv_layer_nd);
                local_node_val = node_matrix[prv_layer][prv_layer_nd].get_node_value();
                voltage = voltage + (local_node_val * multip);
            }
            new_node_val = (1 / (1 + pow(euler, -1 * lambda * voltage)));
            node_matrix[layer][layer_nd].set_node_value(new_node_val);
        }
    }
}

// ------------ MAIN ------------

int main(int argc, char **argv)
{
    // getting neural network structure from csv file

    f_name = "weights.csv";
    csv_read(weight, weight_vec, weight_double, f_name);

    f_name = "structure.csv";
    csv_read(structure, structure_vec, structure_double, f_name);

    // neural network structure creation

    const_set();

    // neural network structure creation

    for (int idx = 0; idx < layer_count; idx++)
    {
        switch (idx)
        {
        case input_layer_num:
            nn_layer_struct(input_node_count);
            break;

        case out_put_layer_num:
            nn_layer_struct(out_put_node_count);
            break;

        default:
            nn_layer_struct(hidden_node_count);
            break;
        }
    }

    // setting network weights

    fill_node_weights();

    // ------ robot initialisations ------

    // create instances

    Aria::init();
    ArRobot robot;

    // parse command line arguments

    ArArgumentParser argParser(&argc, argv);
    argParser.loadDefaultArguments();

    ArRobotConnector robotConnector(&argParser, &robot);

    if (robotConnector.connectRobot())
        std::cout << "Robot connected!" << std::endl;
    robot.runAsync(false);
    robot.lock();
    robot.enableMotors();
    robot.unlock();

    // sonar reading initialization

    ArSensorReading *sonarSensor[8];

    while (true)
    {
        // navigation input

        for (int i = 0; i < 8; i++)
        {
            sonarSensor[i] = robot.getSonarReading(i);
            sonarRange[i] = sonarSensor[i]->getRange();
        }

        // set neural network's input values

        front_distance = ((sin30*sonarRange[3]/5000));
        left_distance = (sonarRange[0]/5000);

        node_matrix[input_layer_num][1].set_node_value(left_distance);
        node_matrix[input_layer_num][2].set_node_value(front_distance);
        
        // output calculation

        feed_forward();

        // set speed values based on neural networks output

        for (int i = 0; i < out_put_node_count; i++)
        {
            output.push_back(node_matrix[out_put_layer_num][i].get_node_value());
        }

        // setting robots speed

        left_speed = (output[lf] * 300);
        right_speed = (output[rght] * 300);

        cout << left_speed << " " << right_speed << " \n";

        robot.setVel2(left_speed, right_speed);
        output.clear();
    }
    return 0;
}