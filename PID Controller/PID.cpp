#include <iostream>
#include <fstream>
#include "Aria.h"
using namespace std;

// initial global variables

double ep = 0;
double ep_perv = 0;
double ei = 0;
double ed = 0;

const double pi = acos(-1);
const double deg_to_rad =  pi/180;
double son_sin[8];

double kp;
double ki;
double kd;

double son_degree[8];
double s[8];
const int sonar_max = 4950;

const double base_speed = 200;

const int base_distance = 450;
const double avoid_dist = 300;

double speed_change;
double distance_min;

int sonarRange[8];

void son_sinf ( double son_degree_2[8])
{
	for(int i = 0 ; i<8 ; i++)
	{
		son_sin[i] = -sin(son_degree_2[i]*deg_to_rad);
	}
}

// minimum sonar readings

double min_son_read(double s[8])
{
	if (son_sin[6]*s[6] < s[7])
		return son_sin[6]*s[6];
	else
		return s[7];
}

// distance calculation

double distancef(double s[8])
{

	distance_min = 4950;

	for (int j = 0; j<8; j++)
	{
		if (min_son_read(s) < distance_min)
			distance_min = min_son_read(s);
	}
	
	return distance_min;
}

// PID function s_default_red

double PID(double s[8])
{

	ep = (base_distance - distancef(s));
	ei += ep;
	ed = ep - ep_perv;
	ep_perv = ep;

	double PID_results;
	const int ei_tresh = 1000;

	kp = 0.3;
	ki = 0.001;
	kd = 0.8;

	// integral anti-windup logic

	if (ei > ei_tresh || ei < -ei_tresh)
	{
		ei = 0;
		PID_results = (kp*ep) + (kd*ed);
	}
	else
	{
		PID_results = (kp*ep) + (ki*ei) + (kd*ed);
	}

	PID_results = (kp*ep) + (ki*ei) + (kd*ed);

	cout << "ei  " << ei << "  ep  " << ep;
	return  PID_results;
}

int main(int argc, char **argv) {

	ofstream wfile;

	// Initialisations

	// create instances

	Aria::init();
	ArRobot robot;

	// parse command line arguments

	ArArgumentParser argParser(&argc, argv);
	argParser.loadDefaultArguments();

	ArRobotConnector robotConnector(&argParser, &robot);

	if (robotConnector.connectRobot())
		std::cout << "Robot connected!" << std::endl;
	robot.runAsync(false);
	robot.lock();
	robot.enableMotors();
	robot.unlock();

	wfile.open("Distance.csv");

	while (true)
	{
		// Sonar Reading initialization

		ArSensorReading *sonarSensor[8];

		// variable initialization

		for (int i = 0; i < 8; i++)
			s[i] = sonar_max;

		// initial sonar degrees

		for (int i = 0; i < 8; i++)
		{
			son_degree[i] = 0;
		}
		
		// initial distance measurment

		for (int i = 0; i < 8; i++)
		{

			if (i > 3)
			{
				sonarSensor[i] = robot.getSonarReading(i);
				sonarRange[i] = sonarSensor[i]->getRange();

				if (sonarRange[i] < sonar_max)
					s[i] = sonarRange[i];
				else
					s[i] = sonar_max;

				// calculating cosine of sonar angles

				son_degree[i] = sonarSensor[i]->getSensorTh(); // Degrees
				son_sinf(son_degree);
			}
			else
				s[i] = sonar_max;
		}

		distancef(s);

		robot.setVel2(base_speed, base_speed);
/* 
		// motion control logic

		// wandering

		if (s[6] > sonar_max - 20 && s[7] > sonar_max - 20 && s[3] > avoid_dist && s[4] > avoid_dist)
		{
			robot.setVel2(base_speed, base_speed);
			cout << "wandering   " << '\n';
			wfile << "wandering   " << endl;
		}

		// obstacle avoidance

		else if (s[3] < avoid_dist || s[4] < avoid_dist)
		{
			robot.setVel2(-base_speed, base_speed);
			cout << "rotate" << '\n';
			wfile << "rotate   " << endl;

		}

		else
		{

			// setting Change in speed

			speed_change = PID(s);

			// setting velocity

			robot.setVel2(base_speed, base_speed + speed_change);

			// test command
			cout << "   s7 & s6  " << s[7] << "  " << son_sin[6]* s[6] <<"   Distance   " << distancef(s) << "   speed_Change   " << speed_change << '\n';

			// keep record of distence ,PID & speed Change
			wfile << "   s7 & s6  ," << s[7] << ",  " << son_sin[6] * s[6] << ",   sonar_min  ," << min_son_read(s) << ",   Distance   ," << distancef(s) << ",   speed_Change   ," << speed_change << endl;

		} 
*/

	}

	wfile.close();

	// termination 
	// stop the robot

	robot.lock();
	robot.stop();
	robot.unlock();

	// terminate all threads and exit
	Aria::exit();

	return 0;
}