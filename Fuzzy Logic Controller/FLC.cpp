// fuzzy logic - 1

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>
#include "Aria.h"

using namespace std;

// ---------- initial variables ----------

int sonar[8];
int sonar_max = 4950;

int avoid_dist = 800;

string logic = "subsumption";

const double pi = acos(-1);
const double deg_to_rad = pi / 180;

// maximum number of inputs & output
int input_num;
int output_max = 5;

// crisp input vectors

vector<int> crisp_sonar_w;
vector<int> crisp_sonar_obs;

// fuzzy input
map<string, double> *fuzzy_in_val;

// Fuzzy set
struct fzs
{
	// membership maps - shapes
	map<string, vector<vector<double>>> in_mm;
	map<string, vector<vector<double>>> out_mm;
};

//---------- fuzzy set membership map ----------

// input vectors - distance
vector<vector<double>> near = {{0, 0}, {1, 500}, {0, 1000}, {0, 50000}};
vector<vector<double>> medium = {{0, 1000}, {1, 1500}, {0, 2000}, {0, 50000}};
vector<vector<double>> far = {{0, 2000}, {1, 3000}, {1, 4999}, {1, 50000}};

// output vectors - speed
vector<vector<double>> slow = {{0, 0}, {1, 60}, {0, 120}};
vector<vector<double>> moderate = {{0, 60}, {1, 180}, {0, 260}};
vector<vector<double>> fast = {{0, 200}, {1, 260}, {0, 300}};

// out put centroids map
map<string, double> centroid_map;

// fuzzy output
vector<vector<string>> active_rules;

// crisp output 
vector<double> crisp_out;

// ---------- rule base arrays ----------

// wall folowing

vector<vector<string>> ru_base;

vector<vector<string>> wall_ru_base = {
	//  s6  -  s7	-  r	-  l
	{"near", "near", "moderate", "slow"},
	{"near", "medium", "moderate", "slow"},
	{"near", "far", "fast", "slow"},
	{"medium", "near", "slow", "moderate"},
	{"medium", "medium", "moderate", "moderate"},
	{"medium", "far", "moderate", "slow"},
	{"far", "near", "moderate", "slow"},
	{"far", "medium", "moderate", "slow"},
	{"far", "far", "fast", "slow"},
};

// obstacle avoidance

vector<vector<string>> obs_ru_base = {
	{"near", "near", "fast", "moderate"},
	{"near", "medium", "moderate", "slow"},
	{"near", "far", "moderate", "slow"},
	{"medium", "near", "slow", "moderate"},
	{"medium", "medium", "moderate", "slow"},
	{"medium", "far", "moderate", "slow"},
	{"far", "near", "moderate", "slow"},
	{"far", "medium", "moderate", "slow"},
	{"far", "far", "fast19", "slow"},
};

// top level rule base - hierarchical fuzzy logic model

// ---------- Functions ----------

// fuzzy input calculator

void fz_fn_cal(string in_mm_name, double input, const int index,vector<double> low, vector<double> high)
{
	if (low[1] < input)
	{
		if (low[0] - high[0] == 0)
		{
			fuzzy_in_val[index][in_mm_name] = high[0];
		}
		else if (low[0] - high[0] > 0)
		{
			fuzzy_in_val[index][in_mm_name] = (input - high[1]) / (low[1] - high[1]);
		}
		else
		{
			fuzzy_in_val[index][in_mm_name] = (low[1] - input) / (low[1] - high[1]);
		}
	}
}

// passing membership map value to fuzzy input calculator

void fu_val_pass(vector<int> crisp_input, map<string, vector<vector<double>>> in_mm)
{
	vector<double> low;
	vector<double> high;

	for (map<string, vector<vector<double>>>::iterator it = in_mm.begin(); it != in_mm.end(); ++it)
	{
		for (unsigned int j = 0; j < it->second.size() - 1; j++)
		{
			low = {it->second[j][0], it->second[j][1]};
			high = {it->second[j + 1][0], it->second[j + 1][1]};

			for (int k = 0; k < crisp_input.size(); k++)
			{
				fz_fn_cal(it->first, crisp_input[k], k,low, high);
			}
		}
	}
}

// // calculate the centroids - input : output vectors - output : centroid values

void centroid(map<string, vector<vector<double>>> fzs_out)
{
	vector<double> mm_center;
	int k = 0;

	for (map<string, vector<vector<double>>>::iterator it = fzs_out.begin(); it != fzs_out.end(); ++it)
	{
		mm_center.push_back((it->second[k][1] + it->second[it->second.size() - 1][1]) / 2);
		k++;
	}

	//reverse(mm_center.begin(), mm_center.end());

	centroid_map["slow"] = mm_center[0];
	centroid_map["moderate"] = mm_center[1];
	centroid_map["fast"] = mm_center[2];
}

// inference engine

void inferencef(map<string, double> *fuzzy_in_val, vector<vector<string>> ru_base)
{
	// inference engine inputs
	vector<vector<string>> ru_in;

	// inference engine outputs
	vector<vector<string>> ru_out;

	vector<string> inner_vect;

	// getting inference engine raw inputs from fuzzy inputs
	for (int k = 0; k < input_num; k++)
	{
		int j = 0;
		for (map<string, double>::iterator it = fuzzy_in_val[k].begin(); it != fuzzy_in_val[k].end(); ++it)
		{
			if (it->second > 0)
			{
				inner_vect.push_back(it->first);
			}
		}
		ru_in.push_back(inner_vect);
		inner_vect.clear();
	}

	// getting inference engine input matrix from raw inputs

	for (auto it_w1 : ru_base)
	{
		inner_vect = it_w1;

		for (auto it_i1 : ru_in[0])
		{
			if (it_i1 == inner_vect[0])
			{
				for (auto it_i2 : ru_in[1])
				{
					if (it_i2 == inner_vect[1])
					{
						ru_out.push_back(inner_vect);
					}
				}
			}
		}
	}

	inner_vect.clear();
	active_rules.clear();

	for (auto it_ro_1 : ru_out)
	{
		for (auto it_ro_2 : it_ro_1)
		{
			string str = it_ro_2;
			inner_vect.push_back(str);
		}
		active_rules.push_back(inner_vect);
		inner_vect.clear();
	}

	for (auto it : active_rules)
	{
		for(auto it2 : it)
		{
			cout << it2 << "  " ;
		}
		cout << " \n ";
	}

}

// defuzzification
// // (fuzzy collective output) aggregation function fl*cl /fl -> firing strength - input : centroids , proper rules + fuzzified input values - output : firing strengths

void defuzz(map<string, double> *fuzzy_in_val, vector<vector<string>> active_rules, map<string, double> centroid_map)
{
	crisp_out = {0, 0};
	vector<vector<int>> centroid_val_vec;
	vector<int> centroid_val;
	vector<double> min_val;
	double min = 0;

	double speed = 0;
	int c_index = 0 ; // centroid index
	int flag = 0; // centroid push back flag

	for (auto it_ar_0 : active_rules)
	{
		flag = 0;
		min = 10000000000000;

		for (int i = 0; i < it_ar_0.size(); i++)
		{
			if (i < input_num)
			{
				if (min > fuzzy_in_val[i][it_ar_0[i]])
				{
					min = fuzzy_in_val[i][it_ar_0[i]];
				}
			}
			else
			{
				centroid_val.push_back(centroid_map[it_ar_0[i]]);
				c_index++;
				flag = 1;
			}
		}
		if (flag == 1)
		{
			centroid_val_vec.push_back(centroid_val);
		}
		min_val.push_back(min);
	}

	for (int i = 0; i < centroid_val_vec.size(); i++)
	{
		speed = 0;

		for (int j = 0; j < min_val.size(); j++)
		{
			speed = speed + (centroid_val_vec[i][j]*min_val[j]);
		}
		crisp_out[i] = speed;
	}

	for (auto it : crisp_out)
		cout << " speed : " << it << " " ;
	cout << "\n";
}

void fuzzy_logic(vector<int> crisp_input, fzs fzs_main, vector<vector<string>> ru_base)
{
	// folowing fuzzification
	fu_val_pass(crisp_input, fzs_main.in_mm);

	// inference
	inferencef(fuzzy_in_val, ru_base);

	// output map centroids
	centroid(fzs_main.out_mm);

	// calculate the crisp output;
	defuzz(fuzzy_in_val, active_rules, centroid_map);
}

int main(int argc, char **argv)
{
	ofstream wfile;

	// ---------- robot initialization ----------

	// create instances
	Aria::init();
	ArRobot robot;

	// parse command line arguments
	ArArgumentParser argParser(&argc, argv);
	argParser.loadDefaultArguments();

	// connect to robot
	ArRobotConnector
		robotConnector(&argParser, &robot);
	if (robotConnector.connectRobot())
		std::cout << "Robot connected!" << std::endl;
	robot.runAsync(false);
	robot.lock();
	robot.enableMotors();
	robot.unlock();

	// fuzzy sets for wall following
	fzs fzs_main;

	// wall following fuzzy sets input initialization - Distance
	fzs_main.in_mm["near"] = near;
	fzs_main.in_mm["medium"] = medium;
	fzs_main.in_mm["far"] = far;

	// wall following fuzzy sets output initialization - Speed
	fzs_main.out_mm["slow"] = slow;
	fzs_main.out_mm["moderate"] = moderate;
	fzs_main.out_mm["fast"] = fast;

	// ---------- main code ----------

	while (true)
	{

		// sonar readings

		ArSensorReading *sonarSensor[8];

		int sonarRange[8];

		for (int i = 0; i < 8; i++)
		{
			if (i > 2 && i != 5)
			{
				sonarSensor[i] = robot.getSonarReading(i);
				sonarRange[i] = sonarSensor[i]->getRange();

				if (sonarRange[i] < sonar_max)
					sonar[i] = sonarRange[i];
				else
					sonar[i] = sonar_max;
			}
			else
				sonarRange[i] = sonar_max;
		}

		ArUtil::sleep(100);

		// getting crisp inputs

		crisp_sonar_w = {sonar[7], sonar[6]};
		crisp_sonar_obs = {sonar[3], sonar[4]};

		if (logic == "subsumption")
		{
			// obstacle avoidance

			if (sonar[3] < avoid_dist || sonar[4] < avoid_dist)
			{
				cout << "obstacle avoidance"
					 << "\n";

				input_num = crisp_sonar_obs.size();
				fuzzy_in_val = new map<string, double>[input_num];
				fuzzy_logic(crisp_sonar_obs, fzs_main, obs_ru_base);

				robot.setVel2(crisp_out[0], crisp_out[1]);
			}

			// wall following

			else
			{
				cout << "wall following"
					 << "\n";

				input_num = crisp_sonar_w.size();
				fuzzy_in_val = new map<string, double>[input_num];

				fuzzy_logic(crisp_sonar_w, fzs_main, wall_ru_base);
				robot.setVel2(crisp_out[0], crisp_out[1]);
			}
		}

		//---------- test ----------

		cout << "\n" << "s6" << "  " << sonar[6] << "  ";
		cout << "s7" << "  " << sonar[7] << "  " << "\n";

		for (int i = 0; i < input_num; i++)
		{
			for (map<string, double>::iterator it = fuzzy_in_val[i].begin(); it != fuzzy_in_val[i].end(); ++it)
			{
				cout << it->first << "  " << it->second << " ";
			}
			cout << "\n";
		}
	}

	// ---------- robot termination ----------

	// stop the robot

	robot.lock();
	robot.stop();
	robot.unlock();

	// terminate all threads and exit

	Aria::exit();
	return 0;
}